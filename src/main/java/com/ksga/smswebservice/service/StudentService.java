package com.ksga.smswebservice.service;

import com.ksga.smswebservice.model.Student;
import java.util.List;

public interface StudentService {
List<Student> findAll();
Student findOne(int id);
public Boolean update(int id,Student student);
public Boolean saveStudent(Student student);
public boolean deleteStudent(int id);
}
