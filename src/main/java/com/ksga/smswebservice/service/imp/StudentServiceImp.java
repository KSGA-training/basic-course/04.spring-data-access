package com.ksga.smswebservice.service.imp;

import com.ksga.smswebservice.model.Student;
import com.ksga.smswebservice.repository.StudentRepository;
import com.ksga.smswebservice.service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImp implements StudentService {
private StudentRepository studentRepository;
@Autowired
  public StudentServiceImp(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  @Override
  public List<Student> findAll() {
    return studentRepository.findAll();
  }

  @Override
  public Student findOne(int id) {
    return studentRepository.findOne(id);
  }

  @Override
  public Boolean update(int id, Student student) {
    return studentRepository.update(id, student);
  }

  @Override
  public Boolean saveStudent(Student student) {
    return studentRepository.insert(student);
  }

  @Override
  public boolean deleteStudent(int id) {
    return studentRepository.delete(id);
  }
}
