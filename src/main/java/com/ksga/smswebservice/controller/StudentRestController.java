package com.ksga.smswebservice.controller;

import com.ksga.smswebservice.model.Student;
import com.ksga.smswebservice.repository.StudentRepository;
import com.ksga.smswebservice.service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/students")
public class StudentRestController {

  private StudentService studentService;
  private StudentRepository studentRepository;
@Autowired
  public void setStudentRepository(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  @Autowired
  public StudentRestController(StudentService studentService) {
    this.studentService = studentService;
  }

  @GetMapping
  public List<Student> findAll(){
    return studentService.findAll();
  }
  @GetMapping("/{id}")
  public Student findById(@PathVariable int id){
  return studentService.findOne(id);
  }
  @PutMapping("/{id}")
  public Student update(@PathVariable int id, @RequestBody Student student){
  if(studentService.update(id,student)){
    return studentService.findOne(id);
  }
  else
    return null;
  }
  @PostMapping
  public String insert(@RequestBody Student student) {
  if(studentService.saveStudent(student))
    return "successfully added";
  else return "fail to add";
  }

  @DeleteMapping("/{id}")
  public String deleteStudent(@PathVariable int id){
  if(studentService.deleteStudent(id))
    return "successfully deleted";
  else return "fail to delete";
  }
  @GetMapping("/show-with-h2")
  public List<Student> getOnlyStudent(){
  return studentRepository.studentOnly();
  }

}
