package com.ksga.smswebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsWebServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(SmsWebServiceApplication.class, args);
  }

}
