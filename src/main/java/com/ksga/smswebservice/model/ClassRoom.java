package com.ksga.smswebservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassRoom {
  private int id;
  public String className;
  public String teacherInCharge;


}
