package com.ksga.smswebservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
  private int id;
  private String name;
  private String gender;
  private String address;
  private String profile;
  public ClassRoom classRoom;



}
