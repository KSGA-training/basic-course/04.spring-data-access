package com.ksga.smswebservice.repository.provider;

import com.ksga.smswebservice.model.Student;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class StudentProvider {

  String tableName ="students";

  public String getAllStudent(){
    return new SQL(){
      {
        SELECT("*");
        FROM(tableName);
        INNER_JOIN("classrooms on students.class_id=classrooms.id");
      }
    }.toString();
  }

  public String selectOne(int id){
    return new SQL(){{
      SELECT("*");
      FROM(tableName);
      WHERE("id=#{id}");
    }}.toString();
  }

  public String insertStudent(@Param("student") Student student){
    return new SQL(){{
      INSERT_INTO(tableName);
    VALUES("name,gender,avartar,address","#{student.name},#{student.gender},#{student.profile},#{student.address}");

    }}.toString();
  }



}
