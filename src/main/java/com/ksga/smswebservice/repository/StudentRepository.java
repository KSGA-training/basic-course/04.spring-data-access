package com.ksga.smswebservice.repository;

import com.ksga.smswebservice.model.Student;
import com.ksga.smswebservice.repository.provider.StudentProvider;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository {

//@Select("select * from students s inner join classrooms on s.class_id=classrooms.id order by s.id asc")
  @SelectProvider(type = StudentProvider.class,method = "getAllStudent")
@Results(id="mappingStudent",value = {
    @Result(property = "profile",column = "avartar"),
    @Result(property = "classRoom.id",column = "class_id"),
    @Result(property = "classRoom.className",column = "class_name"),
@Result(property = "classRoom.teacherInCharge",column = "lead_by")}
)
public List<Student> findAll();

//@Select("select * from students where id = #{id}")
  @SelectProvider(type = StudentProvider.class,method = "selectOne")
@ResultMap("mappingStudent")
public Student findOne(@Param("id") int studentId);


@Delete("delete from students where id=#{id}")
public Boolean delete(int id);

@Update("update students set name= #{student.name}, gender=#{student.gender},address=#{student.address},avartar=#{student.profile} where id =#{id}")
public Boolean update(int id,@Param("student") Student student1);

//@Insert("insert into students (name,gender,avartar,address) values (#{student.name},#{student.gender},#{student.profile},#{student.address})")
@InsertProvider(type = StudentProvider.class,method = "insertStudent")
  public Boolean insert(@Param("student") Student student);
  @Select("select * from students")
  @ResultMap("mappingStudent")
public List<Student> studentOnly();
}
