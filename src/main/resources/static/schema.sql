CREATE TABLE billionaires (
                              id int AUTO_INCREMENT  PRIMARY KEY,
                              first_name VARCHAR(250) NOT NULL,
                              last_name VARCHAR(250) NOT NULL,
                              career VARCHAR(250) DEFAULT NULL
);

CREATE TABLE students (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              name VARCHAR(250) NOT NULL,
                              gender VARCHAR(250) NOT NULL,
                              address VARCHAR(250) DEFAULT NULL,
                              profile VARCHAR(250) DEFAULT NULL
);

insert into students (name, gender, address, profile) values ('Tharin','Male','PP','default.png');